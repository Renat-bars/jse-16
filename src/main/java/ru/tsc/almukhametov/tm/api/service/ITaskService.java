package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void create(String name, String description);

    Task add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> taskComparator);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    boolean existById(String id);

    boolean existByIndex(int index);

    Task startById(String id);

    Task startByIndex(Integer id);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusByName(String name, Status status);

}
