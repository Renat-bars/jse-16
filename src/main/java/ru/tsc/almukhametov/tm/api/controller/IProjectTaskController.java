package ru.tsc.almukhametov.tm.api.controller;

public interface IProjectTaskController {

    void findAllTaskByProjectId();

    void bindTaskByProjectId();

    void unbindTaskByProjectId();

    void removeById();

    void removeByIndex();

    void removeByName();

    void clearProjects();

}
